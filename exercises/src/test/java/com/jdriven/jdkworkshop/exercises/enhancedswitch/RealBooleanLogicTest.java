package com.jdriven.jdkworkshop.exercises.enhancedswitch;

import static org.junit.Assert.*;

import java.io.UncheckedIOException;

import org.junit.Test;


import com.jdriven.jdkworkshop.exercises.enhancedswitch.RealBooleanLogic.RealBoolean;

public class RealBooleanLogicTest {

    private RealBooleanLogic realBooleanLogic = new RealBooleanLogic();

    @Test
    public void realTrueShouldReturnTrue() {
        fail("Do the exercise, then remove this line");
        assertTrue(realBooleanLogic.convertToTraditionalBoolean(RealBoolean.TRUE));
    }

    @Test
    public void realFalseShouldReturnFalse() {
        fail("Do the exercise, then remove this line");
        assertFalse(realBooleanLogic.convertToTraditionalBoolean(RealBoolean.FALSE));
    }

    @Test(expected = UncheckedIOException.class)
    public void realFileNotFoundShouldThrowIOException() {
        fail("Do the exercise, then remove this line");
        realBooleanLogic.convertToTraditionalBoolean(RealBoolean.FILE_NOT_FOUND);
    }

    @Test
    public void realTrueShouldBeTraditionalBooleanValue() {
        fail("Do the exercise, then remove this line");
        assertTrue(realBooleanLogic.isValidTraditionalBoolean(RealBoolean.TRUE));
    }

    @Test
    public void realFalseShouldBeTraditionalBooleanValue() {
        fail("Do the exercise, then remove this line");
        assertTrue(realBooleanLogic.isValidTraditionalBoolean(RealBoolean.FALSE));
    }

    @Test
    public void realFileNotFoundShouldNotBeTraditionalBooleanValue() {
        fail("Do the exercise, then remove this line");
        assertFalse(realBooleanLogic.isValidTraditionalBoolean(RealBoolean.FILE_NOT_FOUND));
    }
}
