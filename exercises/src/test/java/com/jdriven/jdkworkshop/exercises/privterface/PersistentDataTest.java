package com.jdriven.jdkworkshop.exercises.privterface;

import com.complexframework.storage.ClientFactory;
import com.complexframework.storage.DataFormat;
import com.complexframework.storage.StorageClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.assertj.core.api.Fail.fail;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersistentDataTest {

    private static final long STORAGE_ID = 123L;
    private static final DataFormat DATA = new DataFormat();

    @Mock
    private ClientFactory clientFactory;

    @Mock
    private StorageClient storageClient;

    @Spy
    private TestPersistentData persistentData;

    @Before
    public void setup() {
        when(clientFactory.createClient()).thenReturn(storageClient);
        when(storageClient.load(anyLong())).thenReturn(DATA);
    }

    /*
    The interface PersistentData is implemented by classes that can be saved
    and loaded using the StorageClient. The client is part
    of Complex Framework (tm) and should not be changed.

    The code to save and load classes with the client is duplicated in the saveTo and
    loadFrom methods. Get rid of the duplication in the PersistentData interface.

    When you're done either @Ignore this unit test method or comment out the fail() method call
     */
    @Test
    public void readme() {
        //The code already works, make the test fail anyway to get people to read the exercise
        fail("Read the comment above this method!");
    }

    @Test
    public void testCanLoadUsingClient() throws IOException {
        persistentData.loadFrom(clientFactory, STORAGE_ID);
        InOrder inOrder = Mockito.inOrder(clientFactory, storageClient);
        inOrder.verify(clientFactory).createClient();
        inOrder.verify(storageClient).load(STORAGE_ID);
        inOrder.verify(storageClient).getExceptionFromLastAction();
        inOrder.verify(storageClient).shutdown();
        verify(persistentData).fromDataFormat(DATA);
    }

    @Test
    public void testCanSaveUsingClient() throws IOException {
        persistentData.saveTo(clientFactory);

        InOrder inOrder = Mockito.inOrder(clientFactory, storageClient);
        inOrder.verify(clientFactory).createClient();
        inOrder.verify(storageClient).store(DATA);
        inOrder.verify(storageClient).getExceptionFromLastAction();
        inOrder.verify(storageClient).shutdown();
    }

    @Test(expected = IOException.class)
    public void testThrowsExceptions() throws IOException {
        when(storageClient.getExceptionFromLastAction()).thenReturn(new IOException("Exception for unit test"));

        persistentData.loadFrom(clientFactory, STORAGE_ID);
    }

    private static class TestPersistentData implements PersistentData {
        @Override
        public DataFormat toDataFormat() {
            return DATA;
        }

        @Override
        public void fromDataFormat(DataFormat data) {

        }
    }
}
