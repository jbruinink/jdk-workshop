package com.jdriven.jdkworkshop.exercises.patternmatching;

import org.junit.Test;

import static org.junit.Assert.*;

public class ShapeTest {

    @Test
    public void testCircleMovesProperly() {
        Shape original = new Circle(100, 200, 250);
        Shape moved = original.movedBy(50, 25);

        Shape expected = new Circle(150, 225, 350);

        assertEquals(expected, moved);
    }

}