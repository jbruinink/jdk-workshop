package com.jdriven.jdkworkshop.exercises.textblock;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class TypeScriptServiceTest {

    private final TypeScriptService typeScriptService = new TypeScriptService();

    @Test
    public void returnsExpectedTypeScript() throws URISyntaxException, IOException {
        String expected = Files.readString(Paths.get(getClass().getResource("expected.ts").toURI()));
        String actual = typeScriptService.getTypeScript();
        assertEquals(expected, actual);
    }

}