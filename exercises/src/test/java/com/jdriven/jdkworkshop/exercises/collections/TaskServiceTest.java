package com.jdriven.jdkworkshop.exercises.collections;

import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {
    private TaskService taskService;

    @Before
    public void setup() {
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task("Prepare Workshop",
                "Prepare a JDK Workshop by creating slides and exercises.", Duration.ofDays(2)));
        tasks.add(new Task("Shopping", "Pick up some groceries", Duration.ofHours(1)));
        tasks.add(new Task("Work", "Do some work at the office", Duration.ofHours(8)));
        tasks.add(new Task("Dishes", "Put the dishes in the dish washer", Duration.ofMinutes(9)));
        tasks.add(new Task("Prepare Lunch", "Prepare lunch for today", Duration.ofMinutes(3)));
        taskService = new TaskService(tasks);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testTaskListIsUnmodifiable() {
        taskService.getTasks().clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testShortTaskListIsUnmodifiable() {
        taskService.getShortTasks().clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testMapIsUnmodifiable() {
        taskService.getTasksByName().clear();
    }
}
