package com.jdriven.jdkworkshop.exercises.patternmatching;

public class Circle extends Shape {
    private final int radius;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public Shape movedBy(int dX, int dY) {
        return new Circle(x + dX, y + dY, radius);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
