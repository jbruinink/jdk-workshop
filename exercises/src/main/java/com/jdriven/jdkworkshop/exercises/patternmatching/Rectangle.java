package com.jdriven.jdkworkshop.exercises.patternmatching;

public class Rectangle extends Shape {
    private final int width;
    private final int height;

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    @Override
    public Shape movedBy(int dX, int dY) {
        return new Rectangle(x + dX, y + dY, width, height);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
