package com.jdriven.jdkworkshop.exercises.privterface;

import java.io.IOException;


import com.complexframework.storage.ClientFactory;
import com.complexframework.storage.DataFormat;
import com.complexframework.storage.StorageClient;

public interface PersistentData {

    default long saveTo(ClientFactory clientFactory) throws IOException {
        StorageClient client = clientFactory.createClient();
        long result = client.store(toDataFormat());

        IOException exception = client.getExceptionFromLastAction();
        if(exception != null) {
            throw exception;
        }

        client.shutdown();

        return result;
    }

    default void loadFrom(ClientFactory clientFactory, long storageId) throws IOException {
        StorageClient client = clientFactory.createClient();
        DataFormat result = client.load(storageId);

        IOException exception = client.getExceptionFromLastAction();
        if(exception != null) {
            throw exception;
        }

        client.shutdown();

        fromDataFormat(result);
    }

    DataFormat toDataFormat();

    void fromDataFormat(DataFormat data);
}
