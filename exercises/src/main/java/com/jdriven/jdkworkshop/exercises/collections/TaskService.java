package com.jdriven.jdkworkshop.exercises.collections;

import java.util.List;
import java.util.Map;


import com.jdriven.jdkworkshop.util.TODO;

public class TaskService {
    private final List<Task> tasks;

    public TaskService(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * Returns an Unmodifiable copy of the tasks
     * @return an Unmodifiable copy of the tasks
     */
    public List<Task> getTasks() {
        return TODO.implementMe();
    }

    /**
     * Returns an Unmodifiable list of all tasks that take less than 10 minutes
     * @return an Unmodifiable list of all tasks that take less than 10 minutes
     */
    public List<Task> getShortTasks() {
        return TODO.implementMe();
    }

    /**
     * Returns an Unmodifiable map of tasks mapped by their name
     * @return an Unmodifiable map of tasks mapped by their name
     */
    public Map<String, Task> getTasksByName() {
        return TODO.implementMe();
    }
}
