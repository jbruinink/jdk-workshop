package com.jdriven.jdkworkshop.exercises.patternmatching;

public abstract class Shape {
    final int x;
    final int y;

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract Shape movedBy(int dX, int dY);

    @Override
    public String toString() {
        return "Shape{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
