package com.complexframework.storage;

public interface ClientFactory {
    StorageClient createClient();
}
