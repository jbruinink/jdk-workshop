package com.complexframework.storage;

import java.io.IOException;

public interface StorageClient {
    long store(DataFormat dataFormat);

    DataFormat load(long storageId);

    IOException getExceptionFromLastAction();

    void shutdown();
}
