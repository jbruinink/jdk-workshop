package com.jdriven.jdkworkshop.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TryWithResourcesTest {

    @Before
    public void setup() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter("test.txt"));
        writer.println("Hello World!");
        writer.close();
    }

    @After
    public void tearDown() {
        new File("test.txt").delete();
    }

    @Test
    public void test() {
        FileReader in = null;
        try {
            in = new FileReader("test.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        final BufferedReader reader = new BufferedReader(in);
        try (reader) {
            System.out.println(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
