# Workshop Java 9 - 14

This repository contains demos and exercises for getting acquainted with the new features
in Java 9 - 14

Prerequisites for compiling/running the code:

* JDK 14
* Maven
* Your favorite IDE (I used IntelliJ version 2019.3.2)

## Exercises

The exercises are all located in the `com.jdriven.jdkworkshop.exercises` package in the `exercises` module.
Your goal is to fix the TODOs, so that the accompanying unit tests will pass
(of course, the primary goal is to understand the workings of the new features).
The exercises can be performed in any order.

## Jigsaw Exercise

The exercise regarding Project Jigsaw is located in a separate module, called `module-exercise`.

## Demos

Some of the new features are so straight-forward (e.g. the anonymous diamond classes),
or need understanding of some other higher-level concept (e.g. Flow API (reactive programming) and VarHandles).
That is why we haven't created exercises for these specific parts, but the
`com.jdriven.jdkworkshop.demo` package in the `demos` module contains some demos
regarding these new features, for you to explore when desired.
