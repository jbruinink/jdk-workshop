package com.jdriven.jdkworkshop.simpledivision;

import com.jdriven.jdkworkshop.division.DivisionResult;
import com.jdriven.jdkworkshop.division.DivisionService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleDivisionServiceTest {

    private final DivisionService service = new SimpleDivisionService();

    @Test(expected = ArithmeticException.class)
    public void testDoesNotAllowDivideByZero() {
        service.divide(1, 0);
    }

    @Test
    public void testDividingZeroReturnsZero() {
        assertEquals(new DivisionResult(0), service.divide(0, 1));
        assertEquals(new DivisionResult(0), service.divide(0, 2));
    }

    @Test
    public void testWholeNumberQuotient() {
        assertEquals(new DivisionResult(3), service.divide(15, 5));
        assertEquals(new DivisionResult(1), service.divide(1, 1));
        assertEquals(new DivisionResult(7), service.divide(14, 2));
        assertEquals(new DivisionResult(5), service.divide(5, 1));
    }

    @Test
    public void testRemainderQuotient() {
        assertEquals(new DivisionResult(3, 1), service.divide(16, 5));
        assertEquals(new DivisionResult(3, 3), service.divide(18, 5));
        assertEquals(new DivisionResult(3, 3), service.divide(15, 4));
        assertEquals(new DivisionResult(3, 2), service.divide(14, 4));
    }
}