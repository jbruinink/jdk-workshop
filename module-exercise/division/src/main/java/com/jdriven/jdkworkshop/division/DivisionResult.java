package com.jdriven.jdkworkshop.division;

import java.util.Objects;

public class DivisionResult {
    private final int quotient;
    private final int remainder;

    public DivisionResult(int quotient) {
        this(quotient, 0);
    }

    public DivisionResult(int quotient, int remainder) {
        this.quotient = quotient;
        this.remainder = remainder;
    }

    public int getQuotient() {
        return quotient;
    }

    public int getRemainder() {
        return remainder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DivisionResult)) return false;
        DivisionResult that = (DivisionResult) o;
        return quotient == that.quotient &&
                remainder == that.remainder;
    }

    @Override
    public int hashCode() {
        return Objects.hash(quotient, remainder);
    }
}
